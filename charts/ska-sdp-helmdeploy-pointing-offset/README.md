# Pointing Offset Calibration

The  `pointing-offset` chart runs the pointing offset calibration pipeline.
This fits primary beam models to cross-correlation visibilities to estimate
receiver pointing offsets.

For full details of this pipeline see its [documentation](https://developer.skao.int/projects/ska-sdp-script/en/latest/scripts/pointing-offset.html)

The chart currently runs a Kubernetes Job - but this may change!

## Values

The following sections detail the configuration values that can be given to this Chart.

### Selecting Pointing Offset image

| Name              | Description                                                                   | Default                  |
|-------------------|-------------------------------------------------------------------------------|--------------------------|
| `image`           | Overrides the default Docker image of the <br>pointing-offset pipeline to be used | `See Gitlab charts repo` |
| `version`         | Overrides the default image version                                           | `See Gitlab charts repo` |
| `script`          | Overrides the default name of the script                                      | `See Gitlab charts repo` |
| `imagePullPolicy` | Default Docker image  Policy                                                  | `IfNotPresent`           |

### Connecting to the data storage Persistent Volume Claim

At the root level in `values.yaml`, add the `dataProductStorage` key with following value entries:

| Name        | Description                                 | Default     |
|-------------|---------------------------------------------|-------------|
| `name`      | Name of the PVC to connect to               | `test-pvc`  |
| `mountPath` | Path where to mount it within the container | `/mnt/data` |

### Running Pointing offset pipeline

The following keys specify the arguments passed to the pointing offset pipeline
CLI as well as the environmental context for the run.

| Name      | Description                    | Default                                 |
|-----------|--------------------------------|-----------------------------------------|
| `command` | Command to run                 | `pointing-offset`                       |
| `args`    | Command arguments              | `See arguments of the pipeline`         |
| `env`     | Optional environment variables | `See arguments of the pointing script`  |

The command arguments `args` should start with the Pipeline action command
`compute` and include, at minimum, the argument `--msdir=<MS file directory>`.
This is the directory containing the Measurement Sets to process. 

For a list of available arguments of the 
[pointing offset pipeline see the pointing offset command line interface documentation](https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest/api/ska_sdp_wflow_pointing_offset/pointing_offset_cli.html).
