# Changelog

## 1.0.0

* Updated to version 1 and published the chart to CAR

## 0.1.0

* Update the chart to be a deployment ([MR37](https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy-charts/-/merge_requests/37))

## 0.0.1

* Initial version ([MR34](https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy-charts/-/merge_requests/34))

