# Script

The  `script` chart is used to deploy a processing script as a Kubernetes Job.

## Values

| Name                 | Description                                         | Default        |
|----------------------|-----------------------------------------------------|----------------|
| `image`              | OCI image to deploy                                 | `""`           |
| `imagePullPolicy`    | Image pull policy                                   | `IfNotPresent` |
| `env`                | Environment variables to pass to processing script  | `null`         |
| `resources`          | Kubernetes resource requests and limits for the Job | `null`         |
