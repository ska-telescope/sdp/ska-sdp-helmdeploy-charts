# Changelog

## 1.0.0

* Updated to version 1 and published the chart to CAR

## 0.1.0

* First version of the chart ([MR45](https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy-charts/-/merge_requests/45))