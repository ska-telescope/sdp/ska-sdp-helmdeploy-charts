# Test Mock Data

The `mock-data` chart is used to deploy an execution engine that is built from the 
[test-mock-data processing script](https://developer.skao.int/projects/ska-sdp-script/en/latest/test-scripts/test-mock-data.html)
code. This script generates SDP data products without the need to run a full 
observation of any processing pipelines.

## Values

| Name              | Description                                         | Default        |
|-------------------|-----------------------------------------------------|----------------|
| `image`           | OCI image to deploy                                 | `""`           |
| `imagePullPolicy` | Image pull policy                                   | `IfNotPresent` |
| `env`             | Environment variables to pass to processing script  | `null`         |
| `resources`       | Kubernetes resource requests and limits for the Job | `null`         |
| `command`         | Command to execute within the pod                   | `python`       |
| `args`            | Input arguments to the command                      | `null`         |

### Connecting to the data storage Persistent Volume Claim

At the top root level in `values.yaml`, add the `dataProductStorage` and set the following values:

| Name        | Description                                 | Default     |
|-------------|---------------------------------------------|-------------|
| `name`      | Name of the PVC to connect to               | `test-pvc`  |
| `mountPath` | Path where to mount it within the container | `/mnt/data` |