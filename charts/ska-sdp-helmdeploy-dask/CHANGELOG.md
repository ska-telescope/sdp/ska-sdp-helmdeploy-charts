# Changelog

## 1.0.0

* Updated to version 1 and published the chart to CAR


## 0.2.1

* Updated the dask worker-deployment template to allow for custom volumes
and volume claims ([MR19](https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy-charts/-/merge_requests/19))

## 0.2.0

* Jupyter lab notebook deployment added to chart
([MR17](https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy-charts/-/merge_requests/17))

## 0.1.0
Initial version



