{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "receive.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "receive.fullname" -}}
{{- if .Values.fullnameOverride }}
  {{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
  {{- $name := default .Chart.Name .Values.nameOverride }}
  {{- if contains $name .Release.Name }}
    {{- .Release.Name | trunc 63 | trimSuffix "-" }}
  {{- else }}
    {{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
  {{- end }}
  {{- end }}
{{- end }}

{{/*
Create a default fully qualified app name with an index suffix.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "receive.fullnameIndexed" -}}
{{- $ := (index . 0) -}}
{{- $idx := (index . 1) -}}
{{ printf "%s-%02d" ((include "receive.fullname" $) | trunc 60 | trimSuffix "-") $idx }}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "receive.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "receive.labels" -}}
helm.sh/chart: {{ include "receive.chart" . }}
{{ include "receive.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "receive.selectorLabels" -}}
app.kubernetes.io/name: {{ include "receive.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
script: {{ .Values.script }}
{{- end -}}

{{/* Dump all combined environment variables for a container. */}}
{{- define "receive.enriched-env" -}}
{{- $ := (index . 0) -}}
{{- $container_env := (index . 1) | default (list) -}}
{{- $env := $.Values.env -}}
{{- $env = concat $env (list
    (dict "name" "SDP_DATA_PVC_MOUNT_PATH" "value" (index $.Values "data-product-storage" "mountPath"))
    (dict "name" "DATA_PRODUCT_PVC_MOUNT_PATH" "value" (index $.Values "data-product-storage" "mountPath"))
    ) -}}
{{- $env = concat $env $container_env -}}
env:
{{ toYaml $env }}
{{- end -}}

{{/* Default Pod Metadata */}}
{{- define "defaultMetadata" }}
labels:
    {{ include "receive.selectorLabels" . | nindent 4 }}
{{- end -}}

{{/* Final Pod Metadata */}}
{{- define "metadata" -}}
{{- $ := index . 0 -}}
{{- $replicaMetadata := index . 1 -}}
{{- $defaultMetadata := include "defaultMetadata" $ | fromYaml -}}
{{- $metadata := merge $replicaMetadata $defaultMetadata $.Values.extraMetadata -}}
{{ $metadata | toYaml }}
{{- end -}}

{{/* Volume mounts required to use the plasma store */}}
{{- define "receive.plasma-volume-mounts" -}}
{{- if .Values.plasma.enabled -}}
- name: plasma-socket-volume
  mountPath: {{ .Values.plasma.socketVolume.mountPath }}
- name: plasma-shared-memory
  mountPath: /dev/shm
{{- end -}}
{{- end -}}

{{/* The path under which the plasma socket will be visible */}}
{{- define "receive.plasma-socket-path" -}}
{{- printf "%s/%s" .Values.plasma.socketVolume.mountPath "socket" -}}
{{- end -}}

{{- define "all-volume-mounts" -}}
{{ include "receive.plasma-volume-mounts" . }}
- name: data-product-storage
  mountPath: {{ (index .Values "data-product-storage" "mountPath") }}
{{- end -}}

{{- define "receive.full-container" -}}
{{- $ := index . 0 -}}
{{- $container := index . 1 -}}
{{- with $container }}

- name: {{ .name }}
  image: {{ .image }}:{{ .version }}
  imagePullPolicy: {{ .imagePullPolicy | default "IfNotPresent" }}
  {{- if (and .readinessProbe .readinessProbe.file) }}
  {{ with .readinessProbe -}}
  readinessProbe:
    exec:
      command:
      - cat
      - {{ .file }}
    initialDelaySeconds: {{ .initialDelaySeconds }}
    periodSeconds: {{ .periodSeconds }}
  {{- end }}
  {{- end }}
  {{- if .command }}
  command:
  {{- .command | toStrings | toYaml | nindent 2 }}
  {{- end }}
  {{- if .args }}
  args:
  {{- .args | toStrings | toYaml | nindent 2 }}
  {{- end }}
  {{- if .resources }}
  resources:
  {{- toYaml .resources | nindent 4 }}
  {{- end }}
  {{- include "receive.enriched-env" (list $ .env) | nindent 2 }}
  volumeMounts:
  {{- include "all-volume-mounts" $ | nindent 2 }}

{{ end -}}
{{- end -}}

{{/* Ensures the given keys are present in a two-level dictionary */}}
{{- define "receive.set-multi-dict-value" -}}
{{- $dict := index . 0 -}}
{{- $key1 := index . 1 -}}
{{- $key2 := index . 2 -}}
{{- $value := index . 3 -}}
{{- if hasKey $dict $key1 | not -}}
{{- $_ := set $dict $key1 (dict) -}}
{{- end -}}
{{- $group :=get $dict $key1 -}}
{{- $_ := set $group $key2 $value -}}
{{- $dict | toJson -}}
{{- end -}}

{{/* Builds the command for the receiver container */}}
{{- define "receive.receiver-container" -}}
{{- $ := index . 0 -}}
{{- $name := index . 1 -}}
{{- $options := index . 2 -}}
{{- $resources := index . 3 -}}

{{/* Take a copy so our modifications below don't affect other instances! */}}
{{- $receiver := deepCopy $.Values.receiver -}}

{{- if $receiver.readinessProbe.file -}}
{{- $options = (include "receive.set-multi-dict-value" (list $options "reception" "readiness_filename" $receiver.readinessProbe.file) | fromJson) -}}
{{- end -}}
{{- if $.Values.plasma.enabled -}}
{{- $options = (include "receive.set-multi-dict-value" (list $options "consumer" "name" "plasma_writer") | fromJson) -}}
{{- $options = (include "receive.set-multi-dict-value" (list $options "consumer" "plasma_path" (include "receive.plasma-socket-path" $)) | fromJson) -}}
{{- end -}}
{{- $options = merge $options $receiver.options -}}
{{- $command := (list $receiver.executable) -}}
{{- if $receiver.verbose -}}
  {{- $command = append $command "-v" -}}
{{- end -}}
{{- range $group_name, $group_options := $options -}}
{{- range $key, $value := $group_options -}}
  {{- $command = append $command "-o" -}}
  {{- $command = append $command (printf "%s.%s=%s" ($group_name | toString) ($key | toString) ($value | toString)) -}}
{{- end -}}
{{- end -}}
{{- $_ := set $receiver "command" $command -}}
{{- $_ := set $receiver "name" $name -}}
{{- $_ := set $receiver "resources" (merge $resources (default dict $receiver.resources))}}
{{ include "receive.full-container" (list $ $receiver) }}
{{- end -}}

{{/* Builds the difference instances of the receiver containers */}}
{{- define "receive.receiver-containers" -}}
{{- $ := index . 0 -}}
{{- $resources := index . 1 -}}
{{- $instances := $.Values.receiver.instances | int -}}
{{- $streams := $.Values.receiver.streams | int -}}
{{- $port_start := $.Values.receiver.portStart -}}
{{- if gt $instances $streams -}}
  {{- $instances = $streams -}}
{{- end -}}
{{- $safe_streams_per_receiver := div $streams $instances -}}
{{- $remaining_streams := add $streams (mul -1 $instances $safe_streams_per_receiver) -}}
{{- range until $instances -}}
  {{- $receiver_streams := $safe_streams_per_receiver -}}
  {{- if lt . $remaining_streams -}}
    {{- $receiver_streams = add $receiver_streams 1 -}}
  {{- end -}}
  {{- $reception_options := dict "port_start" $port_start "num_streams" $receiver_streams -}}
  {{- $port_start = add $port_start $receiver_streams -}}
  {{- $options := dict "reception" $reception_options -}}
  {{- $name := printf "receiver-%02d" . -}}
  {{ include "receive.receiver-container" (list $ $name $options $resources) }}
{{- end -}}
{{- end -}}
