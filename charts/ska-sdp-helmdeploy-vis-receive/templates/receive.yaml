{{/* vim: set filetype=mustache: */}}
---
# Dummy Service to ensure Pod is DNS addressable
apiVersion: v1
kind: Service
metadata:
  name: {{ include "receive.fullname" $ }}
  namespace: {{ $.Release.Namespace }}
  labels:
    script: {{ $.Values.script }}
spec:
  clusterIP: None
  selector:
    script: {{ $.Values.script }}

{{- range $idx, $pod := $.Values.podSettings }}
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ include "receive.fullnameIndexed" (list $ $idx) }}
  namespace: {{ $.Release.Namespace }}
  labels:
    script: {{ $.Values.script }}
spec:
  replicas: 1
  selector:
    matchLabels:
      script: {{ $.Values.script }}
  serviceName: {{ include "receive.fullname" $ }}
  template:
    metadata:
      {{- include "metadata" (list $ (default dict $pod.extraMetadata)) | nindent 6 }}
    spec:
      {{- $securityContext := merge (default dict $pod.securityContext) $.Values.securityContext }}
      {{- if $securityContext }}
      securityContext:
        {{- $securityContext | toYaml | nindent 8 }}
      {{- end }}

      {{- $podNodeSelector := merge (default dict $pod.nodeSelector) $.Values.nodeSelector }}
      {{- if $podNodeSelector }}
      nodeSelector:
        {{- $podNodeSelector | toYaml | nindent 8 }}
      {{- end }}

      {{- /* ============================================================== */ -}}
      {{- /* Init containers                                                */ -}}
      {{- /*                                                                */ -}}
      {{- /* Init containers have all volumes mounted onto them so they can */ -}}
      {{- /* perform any setup actions required by the receiver or any of   */ -}}
      {{- /* processors down the pipeline                                   */ -}}
      {{- /* ============================================================== */ -}}
      {{- if $.Values.initContainers }}
      initContainers:
      {{- range $.Values.initContainers -}}
      {{ include "receive.full-container" (list $ .) | nindent 6 }}
      {{- end -}}
      {{- end }}

      containers:
      {{- /* ====================== */ -}}
      {{- /* Plasma Store container */ -}}
      {{- /* ====================== */ -}}
      {{- if $.Values.plasma.enabled -}}
      {{- with $.Values.plasma }}
      - name: plasma-store
        image: {{ .image }}:{{ .version }}
        imagePullPolicy: {{ .imagePullPolicy }}
        command:
        - {{ .executable }}
        - -s
        - {{ include "receive.plasma-socket-path" $ }}
        - -m
        - {{ .storeSize | int | quote }}
        volumeMounts:
        {{- include "receive.plasma-volume-mounts" $ | nindent 8 }}
      {{- end -}}
      {{- end -}}

      {{- /* =================== */ -}}
      {{- /* Receiver containers */ -}}
      {{- /* =================== */ -}}
      {{ include "receive.receiver-containers" (list $ (default dict $pod.receiverResources)) | nindent 6 }}

      {{- /* ================================== */ -}}
      {{- /* Plasma-based processors containers */ -}}
      {{- /* ================================== */ -}}
      {{- range $.Values.processors -}}
      {{ include "receive.full-container" (list $ .) | nindent 6 }}
      {{- end -}}

      {{- /* ===================================== */ -}}
      {{- /* Extra containers provided by the user */ -}}
      {{- /* ===================================== */ -}}
      {{- if $.Values.extraContainers -}}
      {{ toYaml $.Values.extraContainers | nindent 6 }}
      {{- end -}}

      {{- /* ================ */ -}}
      {{- /* Required volumes */ -}}
      {{- /* ================ */}}
      volumes:
      - name: data-product-storage
        persistentVolumeClaim:
          claimName: {{ index $.Values "data-product-storage" "name" }}
      {{- if $.Values.plasma.enabled }}
      - name: plasma-socket-volume
        emptyDir:
          medium: Memory
      - name: plasma-shared-memory
        emptyDir:
          medium: Memory
          sizeLimit: {{ $.Values.plasma.storeSize | int }}
		{{- end -}}

{{- end }}