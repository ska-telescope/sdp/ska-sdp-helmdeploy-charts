Deleted charts
==============

The code for the following charts was removed after version 0.1.0 of the
`Helm Charts Repository <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy-charts/-/tree/0.1.0>`_:

- buffer
- cbf-sdp-emulator
- cbf-sdp-multicast-example
- daliuge
- plasma-pipeline
- pss-receive
- receive


Please refer to that tag on the repository if you would like to retrieve/use them.
Their published versions still exist in
`the chart repo <https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy-charts/-/tree/master/chart-repo>`_
for legacy purposes.
For their documentation, follow the link below:

:external+ska-sdp-helmdeploy-charts-010:doc:`Helm Deployer Charts 0.1.0 <index>`
