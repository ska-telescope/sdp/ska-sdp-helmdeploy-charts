SDP Helm Deployer Charts
========================

`Helm Charts <https://helm.sh/docs/topics/charts/>`_  are the package format 
used by the Kubernetes package manager to allocate resources inside a cluster.
The charts used by the :external+ska-sdp-helmdeploy:doc:`SDP Helm deployer <index>` 
are maintained in this repository.


.. toctree::
  :maxdepth: 1
  :caption: Chart development

  create-update

.. toctree::
  :maxdepth: 1
  :caption: Charts in use

  charts/script
  charts/vis-receive
  charts/pointing-offset
  charts/mock-data
  charts/dask


.. toctree::
  :maxdepth: 1
  :caption: Outdated and deleted charts

  deleted-charts
