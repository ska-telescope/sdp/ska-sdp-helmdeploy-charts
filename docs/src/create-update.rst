.. _create_chart:

Creating or updating a chart
============================

The following workflows demonstrate how to create new charts or update existing
ones and publish them with helm. Information about the expected file structure
for charts can be found in the `Helm documentation
<https://helm.sh/docs/topics/charts/#the-chart-file-structure>`_.


Creating a new chart
--------------------

- Create a new branch and create the new chart in a folder
  ``/charts/<new_chart>``. This will contain, at least, ``Chart.yaml``,
  ``CHANGELOG.md``, ``README.md`` and ``values.yaml`` files.
- The ``Chart.yaml`` file should contain a version number and this should be
  recorded in ``CHANGELOG.md``.
- Described the purpose and functionality of the chart in ``README.md``.
- Create a documentation file ``/docs/src/charts/ska-sdp-helmdeploy-<new_chart>.rst`` and link
  this to the ``README.md`` - as for the existing charts.
- Update ``/docs/src/index.rst``.
- Test the chart.
- Create a merge request and get it approved and merged.

Updating an existing chart
--------------------------

- Create a new branch and update the chosen chart as needed.
- Update the version number of your chart in ``Chart.yaml``.
- Document your change in ``CHANGELOG.md``.
- Update the chart ``README.md``, and commit and push the changes.
- Test the chart, make sure it is backwards compatible. If not, dependencies
  are also updated.
- Create a merge request and get it approved and merged.

Chart Repo Directory
--------------------

The chart-repo is now maintained solely for archival and backward compatibility
purposes. New chart versions will no longer be available there. Going forward,
new charts can be found in `Central Artefact Repository (CAR)
<https://artefact.skao.int/repository/helm-internal>`_ 