# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "SDP Helm Deployer Charts"
copyright = "2019-2025 SKA SDP Developers"
author = "SKA SDP Developers"
version = "0.1.0"
release = "0.1.0"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.intersphinx",
    "m2r2",
    "sphinx_copybutton",
]

exclude_patterns = []

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "ska_ser_sphinx_theme"

# -- Extension configuration -------------------------------------------------

# Intersphinx configuration
intersphinx_mapping = {
    "ska-sdp-helmdeploy": ("https://developer.skao.int/projects/ska-sdp-helmdeploy/en/latest", None),
    "ska-sdp-helmdeploy-charts-010": ("https://developer.skao.int/projects/ska-sdp-helmdeploy-charts/en/0.1.0", None),
}
