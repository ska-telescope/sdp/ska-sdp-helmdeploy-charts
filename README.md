# SDP Helm Deployer Charts

The charts used by the Helm deployer are maintained in this repository.

## Creating a new release of the Helm chart

To release a helm chart, create a branch and update the following:
  - In the chart's directory:
    - update the version in the Chart.yaml
    - update the CHANGELOG of the chart
    - update the documentation as needed

Create an MR, get it approved and merge it. Upon merge, the helm-chart-publish job is triggered,
which will build and publish the updated charts. It will only publish the new version
if that version does not yet exist in harbor.skao.int.