-include .make/base.mk
-include .make/helm.mk

HELM_CHARTS_TO_PUBLISH := ska-sdp-helmdeploy-dask \
	ska-sdp-helmdeploy-mock-data \
	ska-sdp-helmdeploy-pointing-offset \
	ska-sdp-helmdeploy-script \
	ska-sdp-helmdeploy-vis-receive

.PHONY: chart-repo

chart-repo: ## Update the packaged charts
	helm package charts/* -d chart-repo
	helm repo index chart-repo
